# Web app

## Variants
Variants of the web app are stored in the variants folder, these exist as patch files

### Switching between variants

To switch between different variants of the web app, there are two scripts:

`apply_variant.sh` will switch the web app to the variant specified, e.g. `apply_variant.sh speedtest`

`reset_variant.sh` will reset the webapp to not use any variant

## Creating an OCI image

### Install and setup buildah

The create-image scripts use the buildah tool, which needs to either be got from the package manager on your OS or built manually

### buildah dependencies

`sudo snap install go --classic`

`sudo apt-get install libgpgme-dev libdevmapper-dev libseccomp-dev`

### Building and installing buildah

```sh
git clone https://github.com/containers/buildah.git
cd buildah
make
sudo make install
```

### Additional setup

Once installed buildah still needs config files added, see the policy.json section at https://podman.io/getting-started/installation

as that page is subject to change at bare minimum we need to create a `/etc/containers/policy.json` containing

```json
{
    "default": [
        {
            "type": "insecureAcceptAnything"
        }
    ]
}
```

This file can also be provided by the containers-common package (provided that it exists in your OS's repos, which it doesn't for ubuntu < 20.10 )

### Creating the image

For the web app, the image can be created using the following command

```bash
# Replace <ARCH> with the architecture neeeded, e.g. amd64, arm, arm64
sudo buildah build -t webapp --arch <ARCH> -f Dockerfile .
```

### Pushing the image to a repo

buildah comes with tools to push to OCI registries , including a local docker registry.
to push to a local docker repo run:

```bash
# Replace <buildah_image_name> with the name buildah gives the image, and <image_name> with the name you want the image to have in the repo
    sudo buildah push <buildah_image_name> docker-daemon:<image_name>:<architecture-tag>
```

and to push to a remote repo:

```bash
sudo buildah push <buildah_image_name>:latest docker://registry.url/<image_name>:<architecture-tag>
```

## Creating an OCI bundle

Once an image has been pushed to a registry, `skopeo` and `umoci` can be used to create a bundle

```bash
skopeo copy docker://registry.url/<image_name>:<architecture-tag> oci:<local-image-dir>:<architecture-tag>

sudo umoci unpack --image <local-image-dir>:<architecture-tag> <local-bundle-dir>
```
