#****************************************************************************
#
# Copyright (c) 2023 Consult Red
#
# Redistribution and use in source and binary forms, with or
# without modification, are permitted provided that the following
# conditions are met:
#
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following
# disclaimer in the documentation and/or other materials provided
# with the distribution.
#
# Subject to the terms and conditions of this license, each
# copyright holder and contributor hereby grants to those receiving
# rights under this license a perpetual, worldwide, non-exclusive,
# no-charge, royalty-free, irrevocable (except for failure to
# satisfy the conditions of this license) patent license to make,
# have made, use, offer to sell, sell, import, and otherwise
# transfer this software, where such license applies only to those
# patent claims, already acquired or hereafter acquired, licensable
# by such copyright holder or contributor that are necessarily
# infringed by:

# (a) their Contribution(s) (the licensed copyrights of copyright
# holders and non-copyrightable additions of contributors, in
# source or binary form) alone; or

# b) combination of their Contribution(s) with the work of
# authorship to which such Contribution(s) was added by such
# copyright holder or contributor, if, at the time the Contribution
# is added, such addition causes such combination to be necessarily
# infringed. The patent license shall not apply to any other
# combinations which include the Contribution.
#
# Except as expressly stated above, no rights or licenses from any
# copyright holder or contributor is granted under this license,
# whether expressly, by implication, estoppel or otherwise.
#
# DISCLAIMER
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
# CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
#****************************************************************************
from random import randint, choice
from datetime import datetime
import argparse
import json

def generate_mock_usage_data(usage_type: str, min_usage: int, max_usage: int) -> dict:
    starting_milli_secs: int = int(datetime.now().timestamp()) * 1000

    if usage_type == "throughput":
        usage_key = f"{usage_type}OverTime"
    else:
        usage_key = f"{usage_type}UsageOverTime"

    usage_over_time_dict = {usage_key: []}

    for i in range(24):
        milli_seconds: int = starting_milli_secs - i * 60 * 60 * 1000
        usage = randint(min_usage, max_usage)
        usage_dict = {"time": milli_seconds, "usage": usage }

        usage_over_time_dict[usage_key].append(usage_dict)

    return usage_over_time_dict

def write_usage_to_file(usage_data, file_path, usage_type):

    if usage_type == "throughput":
        usage_key = f"{usage_type}OverTime"
    else:
        usage_key = f"{usage_type}UsageOverTime"

    try:
        with open(file_path, "r") as file:
            existing_json = json.load(file)
    except FileNotFoundError:
        existing_json = {}

    new_json = existing_json

    new_json[usage_key] = usage_data[usage_key]

    with open(file_path, "w") as file:
        json.dump(new_json, file, indent=4)

parser = argparse.ArgumentParser()
parser.add_argument("usage_type", help="type of usage data", choices=["cpu", "memory", "throughput"])
parser.add_argument("min_usage", help="minimum usage value", type=int)
parser.add_argument("max_usage", help="maximum usage value", type=int)
parser.add_argument("output", help="path to json file", nargs="?")
parser.add_argument("-o", "--output", help="path to json file")

args = parser.parse_args()

usage_data = generate_mock_usage_data(args.usage_type, args.min_usage, args.max_usage)

if args.output:
    write_usage_to_file(usage_data, args.output, args.usage_type)
else:
    print(json.dumps(usage_data))
