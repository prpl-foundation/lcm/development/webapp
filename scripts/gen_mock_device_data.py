#****************************************************************************
#
# Copyright (c) 2023 Consult Red
#
# Redistribution and use in source and binary forms, with or
# without modification, are permitted provided that the following
# conditions are met:
#
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following
# disclaimer in the documentation and/or other materials provided
# with the distribution.
#
# Subject to the terms and conditions of this license, each
# copyright holder and contributor hereby grants to those receiving
# rights under this license a perpetual, worldwide, non-exclusive,
# no-charge, royalty-free, irrevocable (except for failure to
# satisfy the conditions of this license) patent license to make,
# have made, use, offer to sell, sell, import, and otherwise
# transfer this software, where such license applies only to those
# patent claims, already acquired or hereafter acquired, licensable
# by such copyright holder or contributor that are necessarily
# infringed by:

# (a) their Contribution(s) (the licensed copyrights of copyright
# holders and non-copyrightable additions of contributors, in
# source or binary form) alone; or

# b) combination of their Contribution(s) with the work of
# authorship to which such Contribution(s) was added by such
# copyright holder or contributor, if, at the time the Contribution
# is added, such addition causes such combination to be necessarily
# infringed. The patent license shall not apply to any other
# combinations which include the Contribution.
#
# Except as expressly stated above, no rights or licenses from any
# copyright holder or contributor is granted under this license,
# whether expressly, by implication, estoppel or otherwise.
#
# DISCLAIMER
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
# CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
#****************************************************************************
from random import randint, choice
import argparse
import json

def generate_mac_address() -> str:
    hex_digits = [f"{digit:X}" for digit in range(16)]
    return ":".join(choice(hex_digits) + choice(hex_digits) for _ in range(6))

def generate_ip_address() -> str:
    return f"192.168.0.{randint(0,255)}"

def generate_device_name(device_names) -> str:
    device = choice(device_names)
    return f"{device}-{randint(1,256)}"

def generate_mock_devices(num_devices: int, device_names) -> dict:
    used_ip_addresses = set()
    used_mac_addresses = set()
    used_device_names = set()

    devices_dict = {"devices": []}

    for i in range(num_devices):
        ip_address = generate_ip_address()
        mac_address = generate_mac_address()
        device_name = generate_device_name(device_names)
        connection_speed = randint(2, 256)

        while ip_address in used_ip_addresses:
            ip_address = generate_ip_address()

        while mac_address in used_mac_addresses:
            mac_address = generate_mac_address()

        while device_name in used_device_names:
            device_name = generate_device_name(device_names)

        used_ip_addresses.add(ip_address)
        used_mac_addresses.add(mac_address)
        used_device_names.add(device_name)

        device_dict = {"hostname": device_name, "ipAddress": ip_address,
                       "connectionSpeed": connection_speed, "macAddress": mac_address}

        devices_dict["devices"].append(device_dict)

    return devices_dict

def write_usage_to_file(usage_data, file_path):
    with open(file_path, "w") as file:
        json.dump(usage_data, file, indent=4)

parser = argparse.ArgumentParser()
parser.add_argument("num", help="number of devices", type=int)
parser.add_argument("output", help="path to json file", nargs="?")
parser.add_argument("-o", "--output", help="path to json file")
parser.add_argument("-d", "--device", help="devices to choose from", nargs="*", default=["laptop", "raspberry-pi", "smart-tv", "voice-assistant", "phone", "printer"])
args = parser.parse_args()

device_data = generate_mock_devices(args.num, args.device)

if args.output:
    write_usage_to_file(device_data, args.output)
else:
    print(json.dumps(device_data))
