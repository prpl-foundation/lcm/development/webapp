#!/bin/sh
if [ -e .variant ]; then
    variant=$(cat .variant)
else
    echo "no variant applied"
fi

if [ "$variant" ]; then
    patch -s -p1 -R -i variants/$variant.patch
    rm .variant
    echo "variant reset"
fi
