#!/bin/sh
addgroup -g 101 -S nginx
adduser -S -D -H -u 101 -h /var/cache/nginx -s /sbin/nologin -G nginx -g nginx nginx

sed -i 's/index.html/overview.html/' /etc/nginx/conf.d/default.conf

/docker-entrypoint.sh nginx -g "daemon off;"
