FROM docker.io/library/nginx:stable-alpine
COPY nginx.conf  /etc/nginx/nginx.conf
COPY images      /usr/share/nginx/html/images
COPY javascript  /usr/share/nginx/html/javascript
COPY json        /usr/share/nginx/html/json
COPY stylesheets /usr/share/nginx/html/stylesheets
COPY overview.html  /usr/share/nginx/html/
COPY startup.sh /
ENTRYPOINT ["/startup.sh"]

