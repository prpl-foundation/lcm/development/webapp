#!/bin/sh
if [ -z "$1" ]; then
    echo "no variant provided"
    exit
fi

if [ ! -e "variants/$1.patch" ]; then
    echo "variant not found"
    exit
fi

if [ -e .variant ]; then
    variant=$(cat .variant)
fi

if [ "$variant" ]; then
    patch -s -p1 -R -i variants/$variant.patch
    rm .variant
fi

patch -s -p1 -i variants/$1.patch
echo "$1 variant applied"
echo "$1" > .variant
